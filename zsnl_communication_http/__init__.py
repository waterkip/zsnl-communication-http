# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "1.0.11"

import logging
import minty_pyramid
from .routes import routes
from minty.middleware import AmqpPublisherMiddleware
from minty_infra_sqlalchemy import DatabaseTransactionMiddleware
from zsnl_domains import communication

old_factory = logging.getLogRecordFactory()


def log_record_factory(*args, **kwargs):
    record = old_factory(*args, **kwargs)
    record.zs_component = "zsnl_communication_http"
    return record


logging.setLogRecordFactory(log_record_factory)


def main(*args, **kwargs):
    loader = minty_pyramid.Engine(
        domains=[communication],
        command_wrapper_middleware=[
            DatabaseTransactionMiddleware("database"),
            AmqpPublisherMiddleware(
                publisher_name="communication", infrastructure_name="amqp"
            ),
        ],
    )
    config = loader.setup(*args, **kwargs)

    routes.add_routes(config)
    return loader.main()
